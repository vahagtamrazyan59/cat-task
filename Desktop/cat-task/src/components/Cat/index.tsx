import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchCategories, fetchPosts } from '../../redux/action';
import '../../App.css';
import { StoreType, Categories } from '../../types';
import { useNavigate } from 'react-router-dom'
import { setLimit, setIdForCat } from '../../redux/action';


const Cat: React.FC<{ catImageIndex: number, setCatImageIndex: (index: number) => void }> = ({ catImageIndex, setCatImageIndex }) => {
  const dispatch = useDispatch();
  const cats = useSelector((state) => (state as StoreType).cats);
  const categories = useSelector((state) => (state as StoreType).categoria);
  const navigate = useNavigate();
  const limit = useSelector((state) => (state as StoreType).limit);
  const id = useSelector((state) => (state as StoreType).id);
  

  useEffect(() => {
    dispatch(fetchPosts(limit, id) as never);
    dispatch(fetchCategories() as never);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  useEffect(() => {
    dispatch(fetchPosts(limit, (categories?.[id] as {id: number})?.id) as never);
  }, [limit, id, dispatch, categories])

  return (
    <div className='wrapperInCatComp'>
      <div>
        <select onChange={(e) => {
            const index = categories.findIndex((item) => (item as {name: string})?.name === e.target.value);
            dispatch(setIdForCat(index));
          }} className='selectCategories' name="Burger Select">
          {(categories)?.map((item : Categories, index) => (
              <option key={Math.random()} value={item.name}>{item.name}</option>
            ))}
        </select>
        <select onChange={(e) => {
          dispatch(setLimit(+e.target.value));
        }} className='selectLimits' name="Limit Select">
          <option value="10">10</option>
          <option value="20">20</option>
          <option value="50">50</option>
        </select>
      </div>
      <div className='flexCats'>
        {cats?.map((item: {url: string}, index: number) => (
          <div
            key={Math.random()}
            className='cardForCats'
            onClick={() => {
              setCatImageIndex(index);
              navigate(`${catImageIndex}`);
            }}
          >
            <img src={item?.url} alt="Not Loaded" />
          </div>
        ))}
      </div>
    </div>
  )
}

export default Cat