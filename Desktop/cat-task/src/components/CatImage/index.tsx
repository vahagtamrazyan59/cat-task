import React from 'react';
import { useSelector } from 'react-redux';
import { StoreType } from '../../types';

const CatImage: React.FC<{ catImageIndex: number }>  = ({ catImageIndex }) => {
  const cats = useSelector((state) => (state as StoreType).cats);
  return (
    <img src={(cats?.[catImageIndex] as { url: string })?.url} alt="Not Loaded" />
  )
}

export default CatImage;
