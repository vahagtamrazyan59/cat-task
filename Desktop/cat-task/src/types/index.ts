export type StoreType = {
  cats: [];
  categoria: [];
  limit: number;
  id: number;
}

export type Categories = {
  id: number;
  name: string
}