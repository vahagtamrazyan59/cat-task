import React, { useState } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import './App.css';
import Cat from './components/Cat';
import CatImage from './components/CatImage';

function App() {
  const [catImageIndex, setCatImageIndex] = useState<number>(5);

  return (
    <div className='wrapper'>
      <BrowserRouter>
          <Routes>
            <Route path="/" element={<Cat 
              catImageIndex={catImageIndex}
              setCatImageIndex={setCatImageIndex}
            />} />
            <Route path="/:id" element={<CatImage
              catImageIndex={catImageIndex}
            />} />
          </Routes>
        </BrowserRouter>
    </div>
  );
}

export default App;
