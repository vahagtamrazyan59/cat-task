import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import { applyMiddleware, createStore } from "redux"
import postsReducer from './redux/reducer';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';

const store = createStore(postsReducer as never, applyMiddleware(thunk));

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLInputElement);
root.render(
  <Provider store={store}>
    <App />
  </Provider>
);
