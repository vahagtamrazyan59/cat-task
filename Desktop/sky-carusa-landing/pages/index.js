import { Fragment } from 'react';
import Header from '../components/Header'
import Main from '../components/Main'
import Footer from '../components/Footer'

export default function Home() {
  return (
    <div style={{ background: "#000" }}>
      <Header />
      <Main />
      <Footer />
    </div>
  )
}
