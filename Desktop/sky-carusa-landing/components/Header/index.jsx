import { useState, useEffect } from "react";
import styles from "../../styles/Home.module.css"
import Image from "next/image"
import { services } from "../../constants"

const Header = () => {
  const [showBurger, setShowBurger] = useState(false);
  const [w, setW] = useState();
  const scrollingInto = (where) => {
    window.scrollTo({
      top: where,
      behavior: 'smooth',
    });
  }
  useEffect(() => {
    if (typeof window !== 'undefined') {
      console.log(window.innerWidth);
      setW(window.innerWidth);
    }
  }, [])
  return (
    <div className={styles.headerMainDiv}>
      <div className={styles.headerContainer}>
        <div className={styles.headerContainerChild}>
          <div className={styles.headerContainerChild2}>
            <div className={styles.LogoSkyImg} style={{ width: "150px", height: "64px" }}>
              <Image
                src="/images/skyCarLogo.svg"
                alt="Logo"
                width="148px"
                height="64px"
                layout="responsive"
              />
            </div>
            <nav className={styles.navbar}>
              <p onClick={() => location.reload()}>home</p>
              <p onClick={() => {
                (w <= 1280 && w >= 780)
                ? scrollingInto(700)
                : scrollingInto(1200)
              }}>
                services
              </p>
              <p onClick={() => {
                (w <= 1280 && w >= 780)
                ? scrollingInto(3850)
                : scrollingInto(4400)
              }}>
                about us
              </p>
              <p onClick={() => {
                (w <= 1280 && w >= 780)
                ? scrollingInto(3100)
                : scrollingInto(3500)
              }}>
                contact us
              </p>
            </nav>
            <div
              className={
                showBurger
                  ? styles.navbarBurgerNoShow
                  : styles.navbarBurger
              }
              onClick={() => setShowBurger(true)}
            >
              <Image
                src="/images/dropdown.png"
                alt="Dropdown"
                width="40px"
                height="24px"
              />
            </div>
            <div
              className={
                showBurger
                  ? styles.dropdownOpened
                  : styles.dropdownClosed
              }
            >
              <div className={styles.burgerNav}>
                <p onClick={() => location.reload()}>home</p>
                <p onClick={() => {
                  (w <= 780 && w >= 420)
                  ? scrollingInto(600)
                  : scrollingInto(550)
                }}>
                  services
                </p>
                <p onClick={() => {
                  (w <= 780 && w >= 420)
                  ? scrollingInto(3510)
                  : scrollingInto(2200)
                }}>
                  contact us
                </p>
                <p onClick={() => {
                  (w <= 780 && w >= 420)
                  ? scrollingInto(4550)
                  : scrollingInto(3080)
                }}>
                  about us
                </p>
              </div>
              <div className={styles.closeButtonBurger}>
              <Image
                src="/images/burgerClose.svg"
                alt="Close Button"
                width={24}
                height={24}
                onClick={() => setShowBurger(false)}
              />
              </div>
            </div>
          </div>
          <div className={styles.servicesFlex}>
            <div className={styles.heading1Div}>
              <h1>services</h1>
              <div className={styles.headerHrServices}>
                <div className={styles.hrVerticalDiv}>
                  <hr />
                </div>
                <div className={styles.servicesColumn}>
                  {services.map((item, index) => (
                    <div className={styles.servicesMapDiv} key={index}>
                      <div className={styles.servicesImageDiv}>
                        <Image
                          src="/images/blueBall.svg"
                          alt="Ball"
                          width={20}
                          height={20}
                        />
                      </div>
                      <span>{item}</span>
                    </div>
                  ))}
                </div>
              </div>
            </div>
            <div className={styles.rightVisit}>
              <p>Visit our repair center.</p>
              <a href="https://www.google.com/maps" target="blank"><button>Visit</button></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Header;
