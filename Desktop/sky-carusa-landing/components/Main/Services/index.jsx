import styles from "../../../styles/Home.module.css"
import Image from "next/image";

const ServicesBackImage = ({ imageStyle, heading, text }) => (
  <div className={styles[imageStyle]}>
    <div className={styles.servicesImage1Display}>
      <div className={styles.logoInBox}>
        <div className={styles.logoInServices}>
          <Image
            src="/images/skyCarLogoWhite.svg"
            alt="sky Car"
            width={130}
            height={40}
          />
        </div>
      </div>
    </div>
    <div className={styles.servicesImage1Display}>
      <div className={styles.textInServices}>
        <p>{heading}</p>
      </div>
    </div>
    <hr />
    <div className={styles.servicesImage1DisplayText}>
      <div className={styles.textInServices2}>
        <p>{text}</p>
      </div>
    </div>
  </div>
)

const Services = () => {
  return (
    <div className={styles.servicesMainDiv}>
      <div className={styles.headerContainerServices}>
        <div className={styles.headerContainerChild}>
          <h2 className={styles.servicesHeading}>services</h2>
          <div className={styles.servicesLine1}>
            <ServicesBackImage
              imageStyle={"servicesImage1"}
              heading="body shop."
              text="Your car isn’t just an investment,
                it’s something that you rely on every day.
                Getting in an accident is scary,
                but it’s also inconvenient to go about your
                day without your car. That’s why choosing
                where to get your car repaired is an important
                decision. You need to have your car back quickly,
                have the repair done correctly, be sure that it
                is safe to drive, and that the price paid for parts
                and labor is fair."
            />
            <ServicesBackImage
              imageStyle={"servicesImage2"}
              heading="mechanic shop."
              text="Our mechanic performs general maintenance and
                repairs on cars, trucks, small engines and other
                transportation vehicles. our Mechanics can specialize
                in maintenance services such as oil changes or repairs
                to automobile bodies, small engines, tires, diesel engines,
                brakes or transmissions."
            />
          </div>
          <div className={styles.servicesLine2}>
            <ServicesBackImage
              imageStyle={"servicesImage3"}
              heading="electric repaire."
              text="We carry out inspection of vehicle electrical
              problems, repair with the latest technologies,
              replace with the new ones. You will be served by
              our experienced specialists, who will use all their
              skills to quickly restore the car."
            />
            <ServicesBackImage
              imageStyle={"servicesImage4"}
              heading="smog check."
              text="we are Star Certified facility and offer Vehicle
              emission testing (Smog Check) for all type of cars,
              trucks and RVs, be it gas, diesel or hybrid.
              We guarantee the lowest prices. Our technicians are
              state licensed to inspect and certify Vehicles directed
              for both Regular and Star certification."
            />
          </div>
        </div>
      </div>
    </div>
  )
}

export default Services;
