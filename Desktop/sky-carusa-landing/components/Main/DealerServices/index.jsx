import Image from "next/image";
import styles from "../../../styles/Home.module.css";

const DealerServices = () => {
  return (
    <div className={styles.dealerMainDiv}>
      <div className={styles.headerContainerDealer}>
        <div className={styles.headerContainerChild}>
          <h3 className={styles.headingDealer}>dealer service</h3>
          <hr className={styles.dealerHr} />
          <p className={styles.dealerText1}>
            A car dealership, or vehicle local
            distribution, is a business that sells new or
            used cars at the retail level, based on a
            dealership contract with an automaker or
            its sales subsidiary. It can also carry a
            variety of Certified Pre-Owned vehicles. It
            employs automobile salespeople to sell
            their automotive vehicles.
          </p>
          <p className={styles.dealerText2}>
            <span>sky car</span> provides a wide
            range of car dealerships,
            for more information call
            or visit our SKY car service center,
            find your car from our range.
          </p>
          <p className={styles.dealerText3}>
            For questions call.
          </p>
          <div className={styles.dealerPhoneNumber}>
            <div className={styles.phoneImageDiv}>
              <Image
                src="/images/phoneImg.svg"
                alt="Phone number"
                width="30px"
                height="30px"
                className={styles.icon}
              />
            </div>
            <p>+1 747 737 5757</p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default DealerServices;