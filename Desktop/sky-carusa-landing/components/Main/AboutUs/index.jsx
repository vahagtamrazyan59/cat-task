import styles from "../../../styles/About.module.css";
import Image from "next/image";
import { aboutUs } from "../../../constants"

const AboutUs = () => {
  return (
    <div>
      <div className={styles.aboutUsMainDiv}>
        <h2>about us</h2>
      </div>
      <div className={styles.flexAbout}>
        <div className={styles.aboutUsMainDivChild}>
          <div className={styles.aboutFirstFlexDiv}>
            <h4>
              <span>sky car </span>
              will solve your car problems and help save your time,
            </h4>
            {aboutUs.map((item, index) => (
              <div className={styles.aboutMap} key={index}>
                <Image
                  src="/images/blueBall.svg"
                  alt="Blue Ball"
                  width="10px"
                  height="10px"
                />
                <p>{item}</p>
              </div>
            ))}
          </div>
          <div className={styles.aboutSecondFlexDiv}>
            <div className={styles.aboutSecondFlexDivChild}>
              <div className={styles.aboutHorizontalDiv} />
              <p>
                Our company is to ensure the equipment
                of the cars, your safety and time. We make
                <br />
                cars of all types and brands, we make
                the exterior of cars, Chassis replacement,
                <br />
                repair, engine repair.  We also work with the
                car&apos;s electricity, install catalysts fog test
                <br />
                Replacement if necessary.
                <br />
                <span>sky car </span>has extended car sales, rental for all
                makes and fresh cars, we also buy cars
                <br />
                at a good price.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default AboutUs;
