import styles from "../../../styles/Additional.module.css"
import Image from "next/image";
import { vehicleService } from "../../../constants";

const AdditionalService = () => {
  return (
    <div>
      <div className={styles.additionalMain}>
        <div className={styles.additionalMainChild}>
          <h3>additional services</h3>
        </div>
      </div>
      <div className={styles.additionalMain2}>
        <div className={styles.additionalMainChild2}>
          <div className={styles.additionalMainImage}>
            <Image
              src="/images/additionalImg.png"
              alt="Additional Service"
              width="600px"
              height="253px"
            />
          </div>
          <div className={styles.additionalRightPos}>
            <p>
              Our team of experienced specialists strives
              to provide quality service as soon
              <br />
              as possible, offering a solution to
              any issue. We also provide:
            </p>
            {vehicleService.map((item, index) => (
              <div className={styles.vehicleDiv} key={index}>
                <Image
                  src="/images/blueBall.svg"
                  alt="Blue Ball"
                  width="10px"
                  height="10px"
                />
                <p>{item}</p>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  )
}

export default AdditionalService;
