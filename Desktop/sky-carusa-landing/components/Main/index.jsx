import { Fragment } from "react";
import Services from "./Services"
import DealerServices from "./DealerServices"
import ContactUs from "./ContactUs"
import AboutUs from "./AboutUs"
import AdditionalService from "./AdditionalService"

const Main = () => (
  <Fragment>
    <Services/>
    <DealerServices />
    <ContactUs />
    <AboutUs />
    <AdditionalService />
  </Fragment>
)

export default Main;
