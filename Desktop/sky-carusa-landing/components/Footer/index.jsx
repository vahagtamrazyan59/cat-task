import styles from "../../styles/Footer.module.css"
import Image from "next/image";

const Footer = () => {
  return (
    <div className={styles.footerMainDiv}>
      <div className={styles.footerMainImage}>
        <Image
          src="/images/footerCar.png"
          alt="Sky Car"
          width="520px"
          height="470px"
        />
      </div>
        <div className={styles.contactFirstDiv}>
          <h4>contacts</h4>
          <div className={styles.dealerPhoneNumber}>
            <div className={styles.contactPhoneImg}>
              <Image
                src="/images/phoneImg.svg"
                alt="Phone number"
                width="30px"
                height="30px"
              />
            </div>
            <p>+1 747 737 5757</p>
          </div>
          <div className={styles.dealerSMSMessage}>
            <div className={styles.contactPhoneImg}>
              <Image
                src="/images/message.svg"
                alt="Phone number"
                width="30px"
                height="30px"
              />
            </div>
            <p>skycarusa@gmail.com</p>
          </div>
          <div className={styles.contactApp}>
            <div className={styles.contactMeta}>
              <a href="https://www.instagram.com/" target="blank">
                <Image
                  src="/images/instagram.svg"
                  alt="Instagram"
                  width="40px"
                  height="40px"
                />
              </a>
            </div>
            <div className={styles.contactMeta}>
              <a href="https://www.facebook.com/" target="blank">
                <Image
                  src="/images/facebook.svg"
                  alt="Facebook"
                  width="40px"
                  height="40px"
                />
              </a>
            </div>
            <div className={styles.contactMeta}>
              <a href="https://twitter.com/" target="blank">
                <Image
                  src="/images/twitter.svg"
                  alt="Twitter"
                  width="40px"
                  height="40px"
                />
              </a>
            </div>
          </div>
        </div>
        <div>
          <div className={styles.contactFirstDiv2}>
            <h4>address</h4>
            <div className={styles.locationContactAddress}>
              <div className={styles.contactPhoneImg}>
                <Image
                  src="/images/location.svg"
                  alt="Location"
                  width="30px"
                  height="30px"
                />
              </div>
              <p>barkeley street cA 3207.</p>
            </div>
            <div className={styles.rightVisit}>
              <a href="https://www.google.com/maps" target="blank"><button>Visit</button></a>
            </div>
            <h4>work time</h4>
            <div className={styles.locationContactAddress2}>
              <div className={styles.contactPhoneImg}>
                <Image
                  src="/images/workTime.svg"
                  alt="Work Time"
                  width="30px"
                  height="30px"
                />
              </div>
              <p>
                Mon - Fri 9:00AM - 5:00PM
                <br />
                Saturday 10:00AM - 2:00PM
              </p>
            </div>
          </div>
        </div>
      </div>
  )
}

export default Footer;
