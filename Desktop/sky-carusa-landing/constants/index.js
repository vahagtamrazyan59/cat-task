export const services = [
  "BODY SHOP",
  "MECHANIC SHOP",
  "ELECTRIC REPAIR",
  "SMOG CHECK",
  "CAR DEALER",
];

export const aboutUs = [
  "We will respond quickly to your calls and emails.",
  "The specialist will answer all your questions.",
  "The specialist will suggest a solution to your problem.",
  "Will acquaint you with the terms of service.",
];

export const vehicleService = [
  "Vehicle Sales and Leasing.",
  "Vehicle Import and Export."
]