import styles from "../../styles/header.module.scss";
import Image from "next/image";

export default function Header() {
  return (
    <div className={styles.header}>
      <div className={styles.image_container}>
        <Image
          src="/images/pngwing 2.png"
          alt="header photo"
          layout="fill"
          objectFit="contain"
        />
      </div>
      <div className={styles.textblock}>
        <h1>
          Perfect company
          <br /> with perfect solutions.
        </h1>
        <h2>
          Professional and certificates transportation and
          <br /> logistics services.
        </h2>
        <a
          target="blank"
          href="https://www.google.com/maps/place/124+W+Windsor+Rd,+Glendale,+CA+91204,+%D0%A1%D0%A8%D0%90/@34.1364567,-118.2583914,17z/data=!3m1!4b1!4m5!3m4!1s0x80c2c0e32d25ae8d:0xdfe2fbdcaa4578b5!8m2!3d34.1364523!4d-118.2562027"
        >
          <button
            className={styles.textblock_btn}
          >
            Go to map
          </button>
        </a>
      </div>
    </div>
  );
}
