import Services from "./Services";
import Shipping from "./Shipping";
import AboutServices from "./AboutServices";
import ContactUs from "./ContactUs";
import Gallery from "./Gallery";
import AboutUs from "./AboutUs";

const Main = () => (
  <>
    <Services />
    <Shipping />
    <AboutServices />
    <ContactUs />
    <Gallery />
    <AboutUs />
  </>
)

export default Main;
