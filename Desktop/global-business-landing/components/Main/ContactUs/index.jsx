import Image from "next/image";
import styles from "../../../styles/section4.module.scss";

export default function ContactUs() {
  return (
    <div className={styles.container}>
      <div className={styles.mainblock}>
        <div className={styles.block_header}>
          <div className={styles.image_icon}>
            <Image
              src="/images/icon.svg"
              alt="icon"
              layout="fill"
              objectFit="contain"
            />
          </div>
          <h1>Contact Us</h1>
        </div>
        <div className={styles.container_block}>
          <div className={styles.container_img}>
            <Image
              src="/images/Container.png"
              alt="container"
              layout="fill"
              objectFit="contain"
            />
          </div>
          <div className={styles.inf_mainblock}>
            <div className={styles.contacts_mainblock}>
              <h1 className={styles.contacts_header}>Contacts.</h1>
              <div className={styles.contacts_block}>
                <div className={styles.contact_icon}>
                  <Image
                    src="/images/phoneicon.svg"
                    alt="phone icon"
                    layout="fill"
                    objectFit="contain"
                  />
                </div>
                <h2>+1 747 737 5757</h2>
              </div>
              <div className={styles.contacts_block}>
                <div className={styles.contact_icon}>
                  <Image
                    src="/images/mailicon.svg"
                    alt="mail icon"
                    layout="fill"
                    objectFit="contain"
                  />
                  <h4>gublogistics@gmail.com</h4>
                </div>
              </div>
              <div className={styles.contacts_block}>
                <div className={styles.contact_icon}>
                  <Image
                    src="/images/facebookicon.svg"
                    alt="facebook icon"
                    layout="fill"
                    objectFit="contain"
                  />
                </div>
                <p>Global Union Business</p>
              </div>
              <div className={styles.contacts_block}>
                <div className={styles.contact_icon}>
                  <Image
                    src="/images/instagramicon.svg"
                    alt="instagram icon"
                    layout="fill"
                    objectFit="contain"
                  />
                </div>
                <h3>@gublogistics</h3>
              </div>
            </div>
            <div className={styles.address_mainblock}>
              <h1>Address.</h1>
              <div className={styles.address_block}>
                <div className={styles.contact_icon}>
                  <Image
                    src="/images/locationicon.svg"
                    alt="location icon"
                    layout="fill"
                    objectFit="contain"
                  />
                </div>
                <h2>124 W Windsor Rd</h2>
              </div>
              <a
                target="blank"
                href="https://www.google.com/maps/place/124+W+Windsor+Rd,+Glendale,+CA+91204,+%D0%A1%D0%A8%D0%90/@34.1364567,-118.2583914,17z/data=!3m1!4b1!4m5!3m4!1s0x80c2c0e32d25ae8d:0xdfe2fbdcaa4578b5!8m2!3d34.1364523!4d-118.2562027"
              >
                <button className={styles.addressblock_btn}>Go to map</button>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
