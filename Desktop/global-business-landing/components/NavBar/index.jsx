import Image from "next/image";
import React, { useEffect, useState } from "react";
import styles from "../../styles/navbar.module.scss";

export default function NavBar() {
  const [state, setState] = useState(false);
  const [w, setW] = useState();
  useEffect(() => {
    if (typeof window !== 'undefined') {
      console.log(window.innerWidth);
      setW(window.innerWidth);
    }
  }, [])
  const scrollingInto = (where) => {
    window.scrollTo({
      top: where,
      behavior: 'smooth',
    });
  }

  return (
    <nav className={styles.navbar}>
      <div className={styles.logo}>
        <Image
          src="/images/GUB_logo.png"
          layout="fill"
          objectFit="contain"
          alt="logo"
        />
      </div>
      <div className={styles.menu}>
        <ul className={styles.ul}>
          <li className={styles.li}>
            <a onClick={() => {
              (w <= 1280 && w >= 780)
              ? scrollingInto(700)
              : scrollingInto(1000)
            }}>
              services
            </a>
          </li>
          <li>
            <a onClick={() => {
              (w <= 1280 && w >= 780)
              ? scrollingInto(2800)
              : scrollingInto(3300)
            }}>
              contact us
            </a>
          </li>
          <li>
            <a onClick={() => {
              (w <= 1280 && w >= 780)
              ? scrollingInto(3500)
              : scrollingInto(4200)
            }}>
              gallery
            </a>
          </li>
          <li>
            <a onClick={() => {
              (w <= 1280 && w >= 780)
              ? scrollingInto(4350)
              : scrollingInto(5200)
            }}>
              about us
            </a>
          </li>
        </ul>
        <button className={styles.btn}>+1 747 737 5757</button>
      </div>
      <div
        className={state == true ? styles.dropdown_inactive : styles.menubtn}
        onClick={() => setState(true)}
      >
        <div></div>
        <div></div>
        <div></div>
      </div>
      <div
        className={
          state == true ? `${styles.dropdown_active}` : styles.dropdown
        }
      >
        <ul className={styles.dropdown_menu}>
          <li>
            <a onClick={() => {
              (w <= 780 && w >= 420)
              ? scrollingInto(450)
              : scrollingInto(600)
            }}>
              services
            </a>
          </li>
          <li>
            <a onClick={() => {
              (w <= 780 && w >= 420)
              ? scrollingInto(2450)
              : scrollingInto(3300)
            }}>
              contact us
            </a>
          </li>
          <li>
            <a onClick={() => {
              (w <= 780 && w >= 420)
              ? scrollingInto(3100)
              : scrollingInto(4050)
            }}>
              gallery
            </a>
          </li>
          <li>
            <a onClick={() => {
              (w <= 780 && w >= 420)
              ? scrollingInto(3600)
              : scrollingInto(4500)
            }}>
              about us
            </a>
          </li>
        </ul>
        <button className={styles.dropdown_btn}>+1 747 737 5757</button>
        <div className={styles.cancel_btn}>
          <Image
            src="/images/cancel.png"
            alt="X"
            layout="fill"
            objectFit="contain"
            onClick={() => setState(false)}
          />
        </div>
      </div>
    </nav>
  );
}
