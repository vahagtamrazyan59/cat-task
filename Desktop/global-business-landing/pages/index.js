import Head from "next/head";
import Footer from "../components/Footer";
import Header from "../components/Header";
import NavBar from "../components/NavBar";
import Main from "../components/Main";

export default function Home() {
  return (
    <div>
      <Head>
        <title>Global Union Business</title>
      </Head>
      <div className="mainheader">
        <NavBar />
        <Header />
      </div>
      <Main />
      <Footer />
    </div>
  );
}
